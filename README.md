![Renovate banner](https://app.renovatebot.com/images/renovate_660_220.jpg)

# Renovate Bot

The bot uses [renovatebot](https://github.com/renovatebot/renovate/) to automatically create MRs for updating dependencies.
It is running on a [tight schedule](../pipeline_schedules).
The following repositories are currently being updated automatically.
For more info have a look at the [config](./config.js).

## Configuration

Visit [https://docs.renovatebot.com/](https://docs.renovatebot.com/) for documentation, and in particular [https://docs.renovatebot.com/configuration-options/](https://docs.renovatebot.com/configuration-options/) for a list of configuration options.

## Self-Hosting

If you are not on github.com or gitlab.com, or you prefer to run your own instance of Renovate then you have several options:

- Install the `renovate` CLI tool from npmjs, run it on a schedule ( e.g. using `cron` )
- Run the `renovate/renovate` Docker Hub image ( same content/versions as the CLI tool ), run it on a schedule
- Run the `renovate/renovate:slim` Docker Hub image if you only use package managers that don't need third party binaries ( e.g. JS, Docker, Nuget, pip )

[More details on the self-hosting development](https://github.com/renovatebot/renovate/blob/master/docs/development/self-hosting.md).

<!-- rep -->

- [discovery/discovery](https://intgit.gocanvas.com/discovery/discovery/),
- [web/appbuilder](https://intgit.gocanvas.com/web/appbuilder/),
- [web/main](https://intgit.gocanvas.com/web/main/),
- [web/gems/gocanvas-auth](https://intgit.gocanvas.com/web/gems/gocanvas-auth/),
- [web/gems/gocanvas-core](https://intgit.gocanvas.com/web/gems/gocanvas-core/),
- [web/gems/gocanvas-maybe](https://intgit.gocanvas.com/web/gems/gocanvas-maybe/),
- [web/gems/gocanvas-seed](https://intgit.gocanvas.com/web/gems/gocanvas-seed/),
- [web/gems/gocanvas-ui](https://intgit.gocanvas.com/web/gems/gocanvas-ui/),

<!-- rep -->
